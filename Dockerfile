FROM alpine:latest

LABEL name="docker-icecast" \
      maintainer="Sven Bergner sven.bergner@z8r.de" \
      description="Icecast is free server software for streaming multimedia." \
      url="https://icecast.org"

RUN adduser -D -S "icecast"

RUN apk update && \
    apk upgrade && \
    apk add icecast && \
    rm -rf /tmp/* 

EXPOSE 8000

USER icecast

STOPSIGNAL SIGQUIT
ENTRYPOINT ["icecast", "-c", "/config/icecast.xml"]
